# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=difftastic
pkgver=0.26.2
pkgrel=0
pkgdesc="Diff tool that understands syntax"
url="https://difftastic.wilfred.me.uk/"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
arch="$arch !aarch64 !armhf !armv7" # FTBFS on arm
makedepends="cargo openssl-dev"
source="https://github.com/Wilfred/difftastic/archive/$pkgver/difftastic-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/difft -t "$pkgdir"/usr/bin
}

sha512sums="
93ec5059fbf741ca08ce1abef21d0aae83112953379fbd6a657c26af4e6a099351f77553eb31091cb1ffcab2ca1ef2d75b56be62152d2df210236feba8483ed1  difftastic-0.26.2.tar.gz
"
