# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-ipykernel
pkgver=6.11.0
pkgrel=0
pkgdesc="IPython kernel for jupyter"
url="https://github.com/ipython/ipykernel"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-jupyter_client
	ipython
	py3-nest_asyncio
	py3-matplotlib-inline
	py3-tornado
	py3-traitlets
	py3-ipyparallel
	"
checkdepends="py3-pytest py3-flaky py3-pytest-timeout"
source="$pkgname-$pkgver.tar.gz::https://github.com/ipython/ipykernel/releases/download/v$pkgver/ipykernel-$pkgver.tar.gz
	use-packaging.patch"
options="!check" # test suite is incredibly flaky
builddir="$srcdir/ipykernel-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# Many tests fail with pytest.PytestUnraisableExceptionWarning
	# Lots of tests are also flaky, only raising the warning sometimes
	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
69b88e3c8b552f5277f392a004a33a5c64c2edc93aeea0d2c2d03b0d8ee649f41fc1378068c1dd8322807cce188f5579c0d92730760baeca8170a98f585f500b  py3-ipykernel-6.11.0.tar.gz
4939380f733ec84a2a5004229a85af26a4f8d02d97570e8517d8eaaa5a6d1040d43cffd39e259a41f2b06c14560ae075d0251b377732326391a66e7268c5bd90  use-packaging.patch
"
