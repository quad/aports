# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.44.0
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -o bin/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft "$pkgdir"/usr/bin/syft
}

sha512sums="
18fc55c97a33ec89de241bdbf2ca88975054b5c40298740ed85bfa8b0ee08a4d7570158c8b8d60750d21cd2226a5fcc683a47767b5ed596a6104274e5b067c11  syft-0.44.0.tar.gz
"
